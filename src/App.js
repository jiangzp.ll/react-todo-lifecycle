import React from 'react';
import './App.less';
import TodoList from "./components/TodoList";

class App extends React.Component {
    constructor(props, context) {
        super(props,context);
        this.state = {
            showInfo : true
        };
        this.changeListState = this.changeListState.bind(this);
        this.refreshPage = this.refreshPage.bind(this);
    }

    changeListState() {
        this.setState(prevState => ({
            showInfo: !prevState.showInfo
        }));
    }

    refreshPage() {
        this.setState({showAble: true});
        window.location.reload();
    }


    render() {
        return (
            <div className='App'>
                <section className={'buttonComponent'}>
                    <button className={'showInfoButton'} onClick={this.changeListState}>
                        {this.state.showInfo ? 'Show' : 'Hide'}</button>
                    <button className={'refresh'} onClick={this.refreshPage}>refresh</button>
                </section>
                <TodoList show={this.state.showAble}/>
            </div>
        )
    }

}
// const App = () => {
//   return (
//     <div className='App'>
//       <TodoList/>
//     </div>
//   );
// };

export default App;