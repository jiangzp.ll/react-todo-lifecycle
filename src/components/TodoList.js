import React, {Component} from 'react';
import './todolist.less';

class TodoList extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {listCount: 0, countList: []};
    this.addTodoList = this.addTodoList.bind(this);
  }

  render() {
    if (!this.props.show) {
      return (
          <div className={'todoList'}>
            <button className={'addTodoList'} onClick={this.addTodoList}>Add</button>
            <section className={'list'}>
              {this.state.countList.map((count) => {
                return <p key={count}>List Title {count}</p>
              })}
            </section>
          </div>
      );
    } else {
      return <div className={'todoList'}>

      </div>
    }
  }

  addTodoList() {
    this.setState({listCount: this.state.listCount + 1});
    this.setState({countList: [...this.state.countList, this.state.listCount + 1]});
  }

}

export default TodoList;

